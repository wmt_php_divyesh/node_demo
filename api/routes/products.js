const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Product = require('../models/Product');

router.get('/:productID', (req, res, next) => {
    const product_id = req.param.productID;
    Product.findById(product_id)
    .exec()
    .then(doc => {
        console.log(doc);
        if(doc) {
            res.status(200).json(doc);
        } else {
            res.status(404).json({message: 'Data not found.'});
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({error:err});
    });
});

router.post('/', (req, res, next) => {
    // const product = {
    //     name: req.body.name,
    //     price: req.body.price
    // }
    
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price
    });
    product.save()
    .then(result => {
        console.log(result);
        res.status(200).json({
            message: 'Product Added.',
            createdProduct: product
        })
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        })
    });

});

router.delete('/:productID', (req, res, next) => {
    const productID = req.param.productID;
    Product.remove({_id : productID})
    .then(res => {
        res.status(200).json({
            message: 'Product Deleted.'
        })
    })
    .catch(err => {
        res.status(500).json({
            error: err
        })
    });

});

router.patch('/:productID', (req, res, next) => {
    const productID = req.param.productID;
    const updateOps = {};
    for(const ops of req.body) {
        updateOps[ops.propName] = ops.name;
    }
    Product.update({_id : productID}, { $set: updateOps })
    .exec()
    .then(res => {
        res.status(200).json(res)
    })
    .catch(err => {
        res.status(500).json({
            error: err
        })
    });

});

module.exports = router;