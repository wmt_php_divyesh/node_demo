const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const productRoutes = require('./api/routes/products');

mongoose.connect("mongodb+srv://root:"+ 
    process.env.MONGO_ATLAS_PW + 
    "@node-api-cluster-fsjq9.mongodb.net/test?retryWrites=true&w=majority", {
        userMongoClient : true
    });

// Logger
app.use(morgan('dev'));

// Bodyparser 
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

// Routes which handle request
app.use('/products', productRoutes);

// Error Handling
app.use((req, res, next) => {
    const error = new Error('Invalid Request.');
    error.status = 404;
    next(error);
})

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    })
});

module.exports = app;